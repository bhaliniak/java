package Searcher;

import java.util.List;

/**
 * Created by pmackowiak on 27.09.2017.
 */
public interface FilterIssue {

    List<String> getResolvedByDevOrQaTaskKeysFromMyProject();

}
