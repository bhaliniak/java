package Searcher;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by dd on 27.09.2017.
 */
public class FilterIssueImpl implements FilterIssue {

    private IssueRepository issueRepository;

    public FilterIssueImpl(IssueRepository issueRepository){
        this.issueRepository = issueRepository;
    }

    public List<String> getResolvedByDevOrQaTaskKeysFromMyProject() {
        //getAllIssuesFromDatabase
        List<Issue> allIssues = new ArrayList<Issue>();
        allIssues = this.issueRepository.getAllIssuesFromDatabase();

        //filter by project
        List<Issue> issueInMyProject = new ArrayList<Issue>();
        for(int i=0;i<allIssues.size();i++){
            if(allIssues.get(i).equals("MyProject")){
                issueInMyProject.add(allIssues.get(i));
            }
        }

        //filter by issueType
        List<Issue> taskIssueType = new ArrayList<Issue>();
        for(int i=0;i<issueInMyProject.size();i++){
            if(issueInMyProject.get(i).equals("Task")){
                taskIssueType.add(issueInMyProject.get(i));
            }
        }

        //filter issues by resolved
        List<Issue> list3 = new ArrayList<Issue>();
        for(int i=0;i<taskIssueType.size();i++){
            if(taskIssueType.get(i).getResolved() == true){
                if(resolvedByDev(taskIssueType.get(i)) == true || resolvedByQa(taskIssueType.get(i)) == true) {
                    list3.add(taskIssueType.get(i));
                }
            }
        }

        //get keys from issues
        List<String> list4 = new ArrayList<String>();
        list4 = list3.stream().map(x -> x.getKey()).collect(Collectors.toList());
        return list4;

    }

    public Boolean resolvedByDev(Issue resolvedIssues){
        Boolean isResolvedByDev;
        isResolvedByDev = resolvedIssues.getResolvedBy().equals("Dev");
        return isResolvedByDev;
    }

    public Boolean resolvedByQa(Issue resolvedIssues){
        Boolean isResolvedByQa;
        isResolvedByQa = resolvedIssues.getResolvedBy().equals("Qa");
        return isResolvedByQa;
    }

}
