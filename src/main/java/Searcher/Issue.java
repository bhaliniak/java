package Searcher;

import java.util.List;

/**
 * Created by pmackowiak on 27.09.2017.
 */
public class Issue {

    private Long id;

    private String key;

    private String issueType;

    private String project;

    private Boolean isResolved;

    private String resolvedBy;

    private List<Long> linkedIssuesIds;

    public Issue(Long id, String key, String issueType, String project, Boolean isResolved, String resolvedBy, List<Long> linkedIssuesIds){
        this.id = id;
        this.key = key;
        this.issueType = issueType;
        this.project = project;
        this.isResolved = isResolved;
        this.resolvedBy = resolvedBy;
        this.linkedIssuesIds = linkedIssuesIds;
    }

    public String getIssueType() {
        return issueType;
    }

    public void setIssueType(String issueType) {
        this.issueType = issueType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public Boolean getResolved() {
        return isResolved;
    }

    public void setResolved(Boolean resolved) {
        isResolved = resolved;
    }

    public String getResolvedBy() {
        return resolvedBy;
    }

    public void setResolvedBy(String resolvedBy) {
        this.resolvedBy = resolvedBy;
    }

    public List<Long> getLinkedIssuesIds() {
        return linkedIssuesIds;
    }

    public void setLinkedIssuesIds(List<Long> linkedIssuesIds) {
        this.linkedIssuesIds = linkedIssuesIds;
    }
}
